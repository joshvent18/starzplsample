//
//  SpinnerView.swift
//  ZPLSample
//
//  Created by Joshua Ventocilla on 6/4/24.
//

import Foundation
import UIKit

fileprivate var activityView: UIView?

extension UIViewController {
        
    func showActivitySpinner() {
        activityView = UIView(frame: self.view.bounds)
        activityView?.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        let spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        spinner.center = activityView!.center
        spinner.startAnimating()
        activityView?.addSubview(spinner)
        self.view.addSubview(activityView!)
    }
    
    func showDiscoverySpinner() {
        activityView = UIView(frame: self.view.bounds)
        activityView?.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        let textLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 66))
        textLabel.text = "Discovering.."
        textLabel.textAlignment = .right
        textLabel.font = UIFont(name: "Avenir Medium", size: 20)
        textLabel.center = CGPoint(x: activityView!.center.x-100, y: activityView!.center.y)
        
        let spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        spinner.center = CGPoint(x: activityView!.center.x-55, y: activityView!.center.y)
        spinner.startAnimating()
        activityView?.addSubview(spinner)
        activityView?.addSubview(textLabel)
        self.view.addSubview(activityView!)
    }
    
    func showPrintingSpiner() {
        activityView = UIView(frame: self.view.bounds)
        activityView?.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        let textLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 66))
        textLabel.text = "Printing.."
        textLabel.textAlignment = .right
        textLabel.font = UIFont(name: "Avenir Medium", size: 20)
        textLabel.center = CGPoint(x: activityView!.center.x-100, y: activityView!.center.y)
        
        let spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        spinner.center = CGPoint(x: activityView!.center.x-55, y: activityView!.center.y)
        spinner.startAnimating()
        activityView?.addSubview(spinner)
        activityView?.addSubview(textLabel)
        self.view.addSubview(activityView!)
    }
    
    func removeSpinner() {
        activityView?.removeFromSuperview()
        activityView = nil
    }
    
}
