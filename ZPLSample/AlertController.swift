//
//  AlertController.swift
//  ZPLSample
//
//  Created by Joshua Ventocilla on 6/5/24.
//

import Foundation
import UIKit

extension UIViewController {
    
    func alertNoDevicesFound() {
        let confirm = UIAlertController(title: "No Devices Found", message: "Unable to find a device. Make sure that devices are turned on and available to connect.", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            // do nothing
        })
    
        confirm.addAction(ok)
        
        self.present(confirm, animated: true, completion: nil)
    }
        
    func alertDeviceNotConnected() {
        let alert = UIAlertController(title: "Device Not Connected", message: "Connect the device to start printing", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
           
        })

        alert.addAction(ok)

        self.present(alert, animated: true, completion: nil)
    }
    
    func alertDeviceError(message: String) {
        let confirm = UIAlertController(title: message, message: "Please check the printer", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            // do nothing
        })
    
        confirm.addAction(ok)
        
        self.present(confirm, animated: true, completion: nil)
    }
    
    func alertZPLError(message: String) {
        let confirm = UIAlertController(title: message, message: "Make sure the input is in valid ZPL commands and try again.", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            // do nothing
        })
    
        confirm.addAction(ok)
        
        self.present(confirm, animated: true, completion: nil)
    }
        
}

