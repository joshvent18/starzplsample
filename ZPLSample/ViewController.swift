/*
 ViewController.swift
 ZPLSample

 Sample application to print a label using ZPL commands for mC-Label3 using
 Labelary API
 
 API Information: https://labelary.com/service.html
 Created by Joshua Ventocilla on 4/22/24.
*/

import UIKit
import StarIO10

class ViewController: UIViewController {
    
    @IBOutlet weak var discoveryButton: UIBarButtonItem!
    
    var starConnectionSettings: StarConnectionSettings?
    var labelAPI: String?
    var identifier: String?
    var printerIdentifier: String?
    var printerInterface: InterfaceType?
    var printerModel: String?
    var pickerData: [String] = [String]()
    
    var zpl = ""
    
    @IBOutlet weak var zplHeaderLabel: UILabel!
    @IBOutlet weak var zplCommandLabel: UILabel!
    @IBOutlet weak var printerStatusLabel: UILabel!
    @IBOutlet weak var printerIdentifierLabel: UILabel!
    @IBOutlet weak var printerModelLabel: UILabel!
    
    @IBOutlet weak var printButton: UIButton!
    @IBOutlet weak var zplLabelPicker: UIPickerView!
    @IBOutlet weak var zplTextView: UITextView!
    
    let networkRequest: NetworkManager = NetworkManager()
    private var labelaryAPI = "https://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/"
    
    private var manager: StarDeviceDiscoveryManager? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        pickerData = ["Simple Label", "QR Code Label", "Shipping Label", "Input ZPL Commands"];
        
        zplLabelPicker.delegate = self
        zplLabelPicker.dataSource = self
        zplTextView.delegate = self
        
        setZPLDefault()
        
    }

    override func viewDidAppear(_ animated: Bool) {
        if printerIdentifier != nil {
            printerIdentifierLabel.text = "Printer identifier: \(printerIdentifier!)"
            printerModelLabel.text      = "Printer model: \(printerModel!)"
            printerStatusLabel.textColor = UIColor.systemGreen
            printerStatusLabel.text = "CONNECTED"
        } else {
            printerIdentifierLabel.text = "Printer identifier: unknown"
            printerModelLabel.text      = "Printer model: unknown"
            printerStatusLabel.textColor = UIColor.systemRed
            printerStatusLabel.text = "DISCONNECTED"
        }
    }
    
    func setZPLDefault() {
        zplLabelPicker.selectRow(0, inComponent: 0, animated: true)
        zpl = "^XA^FXfield for the element 'Sample Text Element'^FO200,320,2^FWN^A40,40^FDSample Text Element!^FS^FXfield for the element 'Sample Barcode Element'^FO200,640,2^FWN^BY4,,64^BCN,64,Y,N^FD123456^FS^XZ"
        zplTextView.text = zpl
        labelAPI = labelaryAPI + zpl
    }
    
    func printImage(completion: @escaping (Data?, HTTPURLResponse) -> ()) {
        let url = URL(string: labelAPI!)!
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("image/png", forHTTPHeaderField: "Accept")
        
        /*
            API call
         */

        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                        
            if let httpResponse = response as? HTTPURLResponse {
                switch httpResponse.statusCode {
                case 200:
                    guard let imageData = data else {
                        completion(nil, httpResponse)
                        return
                    }
                    
                    completion(imageData, httpResponse)
                    break
                case 404:
                    completion(nil, httpResponse)
                    break
                default:
                    completion(nil, httpResponse)
                    return
                }
            }
        }).resume()
    }
    
    @IBAction func printButtonPressed(_ sender: Any) {
        if printerIdentifier == nil {
            alertDeviceNotConnected()
        } else {
            showActivitySpinner()
            printImage() { imageData, urlResponse in
                
                if urlResponse.statusCode == 200 {
                    guard let label = imageData else {
                        return
                    }
                    
                    let imageUse = UIImage(data: label)
                    
                    DispatchQueue.main.async {
                        self.removeSpinner()
                        self.transitionPreview(image: imageData!)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.removeSpinner()
                        self.alertZPLError(message: "Invalid ZPL Commands")
                    }
                }
                                
            } // end if
        } // end printButtonPressed()
        
    }
    
    @IBAction func discoveryButtonPressed(_ sender: Any) {
        let searchSB = UIStoryboard.init(name: "SearchPrinter", bundle: nil)
        let searchVC = searchSB.instantiateViewController(withIdentifier: "SearchPrinterBoard") as! SearchViewController
                                            
        searchVC.searchViewDelegate     = self
        searchVC.modalTransitionStyle   = .flipHorizontal
        searchVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func transitionPreview(image: Data) {
        let previewSB = UIStoryboard.init(name: "PrintPreview", bundle: nil)
        let previewVC = previewSB.instantiateViewController(withIdentifier: "PreviewBoard") as! PreviewViewController
                                            
        previewVC.imageData = image
        previewVC.printerID = printerIdentifier
        previewVC.interface = printerInterface
        
        previewVC.modalTransitionStyle   = .flipHorizontal
        previewVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(previewVC, animated: true)
    }
    
}

extension ViewController: SearchViewDelegate {
    func printerInformation(model: String, identifier: String, interface: InterfaceType) {
        printerModel = model
        printerIdentifier = identifier
        printerInterface = interface
    }
}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch row { // pick is selected and change accordingly
        case 0:
            labelaryAPI = "https://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/"
            zpl = "^XA^FXfield for the element 'Sample Text Element'^FO200,320,2^FWN^A40,40^FDSample Text Element!^FS^FXfield for the element 'Sample Barcode Element'^FO200,640,2^FWN^BY4,,64^BCN,64,Y,N^FD123456^FS^XZ"
            zplTextView.text = zpl
            zplTextView.isEditable = false
            zplTextView.textColor  = .darkGray
            zplTextView.backgroundColor = .systemGray6
            break
        case 1:
            labelaryAPI = "https://api.labelary.com/v1/printers/8dpmm/labels/4x3/0/"
            zpl = "^XA^FO100,63^ABB,30,18^FDUNITED STATES^FS^FO600,130^ABB,30,18^FDSHELF47^FS^FO50,63^ABB,30,18^FDNEW JERSEY^FS^FO170,20^ABN,30,18^FDPRINTER MODEL^FS^FO170,70^ABN,30,18^FDMCLABEL32^FS^FO170,110^BY4,2.0,65^BQN,2,10^FD0933860^FS^FO700,130^ABN,30,18^FD1/46^FS^FO700,210^ABN,30,18^FDA2^FS^FO700,170^ABN,30,18^FD403^FS^FO850,160^ABN,20,14^FD50^FS^XZ"
            zplTextView.text = zpl
            zplTextView.isEditable = false
            zplTextView.textColor  = .darkGray
            zplTextView.backgroundColor = .systemGray6
            break
        case 2:
            labelaryAPI = "https://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/"
            zpl = "^XA^FX Top section with logo, name and address.^CF0,60^FO50,50^GB100,100,100^FS^FO75,75^FR^GB100,100,100^FS^FO93,93^GB40,40,40^FS^FO220,50^FDStarShipping, Inc.^FS^CF0,30^FO220,115^FD65 Clyde Road Suite G^FS^FO220,155^FDSomerset, NJ 08873^FS^FO220,195^FDUnited States (USA)^FS^FO50,250^GB700,3,3^FS^FX Second section with recipient address and permit information.^CFA,30^FO50,300^FDJohn Doe^FS^FO50,340^FD100 Main Street^FS^FO50,380^FDSpringfield TN 39021^FS^FO50,420^FDUnited States (USA)^FS^CFA,15^FO600,300^GB150,150,3^FS^FO638,340^FDPermit^FS^FO638,390^FD123456^FS^FO50,500^GB700,3,3^FS^FX Third section with bar code.^BY5,2,270^FO100,550^BC^FD12345678^FS^FX Fourth section (the two boxes on the bottom).^FO50,900^GB700,250,3^FS^FO400,900^GB3,250,3^FS^CF0,40^FO100,960^FDCtr. X34B-1^FS^FO100,1010^FDREF1 F00B47^FS^FO100,1060^FDREF2 BL4H8^FS^CF0,190^FO470,955^FDCA^FS^XZ"
            zplTextView.text = zpl
            zplTextView.isEditable = false
            zplTextView.textColor  = .darkGray
            zplTextView.backgroundColor = .systemGray6
        case 3:
            labelaryAPI = "https://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/"
                        
            // MANUAL INPUT
            zplTextView.text = "Input ZPL commands.."
            zplTextView.isEditable = true
            zplTextView.textColor  = .lightGray
            zplTextView.backgroundColor = .systemGray6
            zpl = zplTextView.text
        default:
            labelaryAPI = "https://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/"
            zpl = "^^XA^FXfield for the element 'Sample Text Element'^FO200,320,2^FWN^A40,40^FDSample Text Element!^FS^FXfield for the element 'Sample Barcode Element'^FO200,640,2^FWN^BY4,,64^BCN,64,Y,N^FD123456^FS^XZ"
            zplTextView.text = zpl
            zplTextView.isEditable = false
            zplTextView.textColor  = .darkGray
            zplTextView.backgroundColor = .systemGray6
        }
        
        labelAPI = labelaryAPI + zpl
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        zplTextView.textColor  = .black
        zplTextView.text = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        zpl = zplTextView.text
        labelAPI = labelaryAPI + zpl
    }
        
}
