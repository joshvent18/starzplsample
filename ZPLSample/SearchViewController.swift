//
//  SearchViewController.swift
//  ZPLSample
//
//  Created by Joshua Ventocilla on 5/23/24.
//

import UIKit
import StarIO10

protocol SearchViewDelegate: AnyObject {
    func printerInformation(model: String, identifier: String, interface: InterfaceType)
}

class SearchViewController: UIViewController, StarDeviceDiscoveryManagerDelegate {
    
    var isFound: Bool = false
    var selectedInterfaceType : InterfaceType = InterfaceType.unknown
    var interfaceTypeArray: [InterfaceType] = []
    
    @IBOutlet weak var printerFoundLabel: UILabel!
    @IBOutlet weak var printerIdentifierLabel: UILabel!
    @IBOutlet weak var printerModelLabel: UILabel!
    
    @IBOutlet weak var discoveryButton: UIButton!
    @IBOutlet weak var interfaceControl: UISegmentedControl!
    
    private var manager: StarDeviceDiscoveryManager? = nil
    
    weak var searchViewDelegate: SearchViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        printerFoundLabel.text  = "Printer Not Found"
        
        interfaceControl.selectedSegmentIndex = 0
        
        if !isFound {
            printerIdentifierLabel.isHidden = true // no printer found
        } else {
            printerIdentifierLabel.isHidden = false // reveal
        }
        
    }
        
    @IBAction func discoveryButtonPressed(_ sender: Any) {
        
        // For this example we'll only use the ethernet interface since we will be calling an API anyways, but any interface
        // can be added.
        switch interfaceControl.selectedSegmentIndex {
        case 0:
            selectedInterfaceType = InterfaceType.lan
            interfaceTypeArray.append(InterfaceType.lan)
            print("LAN INTERFACE")
        case 1:
            selectedInterfaceType = InterfaceType.bluetooth
            interfaceTypeArray.append(InterfaceType.bluetooth)
            print("BLUETOOTH INTERFACE ")
        default:
            return
        }
        
        showActivitySpinner()
        manager?.stopDiscovery()
        
        do {
            try manager = StarDeviceDiscoveryManagerFactory.create(interfaceTypes: interfaceTypeArray)
            manager?.discoveryTime = 10000
            manager?.delegate = self
            try manager?.startDiscovery()
        } catch StarIO10Error.illegalDeviceState(message: let message, errorCode: let errorCode) {
            
            if errorCode == StarIO10ErrorCode.bluetoothUnavailable {
                // Example of error: Bluetooth capability of iOS device is disabled.
                // This may be due to the host device's Bluetooth being off.
            }
            
        } catch let error {
            removeSpinner()
        }
    }

    //MARK: - StarIO Delegates
    func manager(_ manager: any StarIO10.StarDeviceDiscoveryManager, didFind printer: StarIO10.StarPrinter) {
        
        guard let model = printer.information?.model else {
            return
        }
        
        searchViewDelegate?.printerInformation(model: model.description, identifier: printer.connectionSettings.identifier, interface: printer.connectionSettings.interfaceType)
        
        DispatchQueue.main.async {
            
            self.isFound = true
            self.printerIdentifierLabel.isHidden = false
            self.printerIdentifierLabel.text = "Identifer: \(printer.connectionSettings.identifier)"
            self.printerModelLabel.text      = "Model: \(model)"
            
            self.printerFoundLabel.text = "Printer Found"
            self.printerFoundLabel.textColor = UIColor.systemGreen
        
            manager.stopDiscovery()
        }
        
    }
    
    func managerDidFinishDiscovery(_ manager: any StarIO10.StarDeviceDiscoveryManager) {
        
        DispatchQueue.main.async {
            self.removeSpinner()
        }
    }
        
}
