//
//  NetworkManager.swift
//
//  Created by Joshua Ventocilla on 4/22/24.
//

import UIKit

/*
    This is the network class that will handle all API requests
    from: paulpeelen.com/SwiftNetworkManager
 */
class NetworkManager {
    
    // Errors the class might return
    enum ManagerErrors: Error {
        case invalidResponse
        case invalidStatusCode(Int)
    }
    
    // the request method
    enum HttpMethod: String {
        case get
        case post
        
        var method: String { rawValue.uppercased() }
    }
    
    /*
        Request data from endpoint
        - Parameters:
            - url: the URL
            - httpMethod: The HTTP Method to use, either get or post in this case
            - completion: The completion closure, returning a Result of either the generic type or an error
     */
    func request<T: Decodable>(fromURL url: URL, httpMethod: HttpMethod = .get, completion: @escaping(Result<T, Error>) -> Void) {
        let completionOnMain: (Result<T, Error>) -> Void = { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.method
        
        let urlSession = URLSession.shared.dataTask(with: request) { data, response, error in
            // First check if we got an error, if so we are not interested int he response or data.
            // Remember, and 404, 500, 501 http error code does not result in an error in URLSession,
            // it will only return an error here in case of e.g. Network timeout.
            if let error = error {
                completionOnMain(.failure(error))
                return
            }
            
            // Lets check the status code, we are only interested in results between 200 and 300 in statuscode. If the statuscode is anything
            // else we want to return the error with the statuscode that was returned. In this case, we do not care about the data.
            guard let urlResponse = response as? HTTPURLResponse else {
                return completionOnMain(.failure(ManagerErrors.invalidResponse))
            }
            
            if !(200..<300).contains(urlResponse.statusCode) {
                print(urlResponse.statusCode)
                print(urlResponse.description)
                return completionOnMain(.failure(ManagerErrors.invalidStatusCode(urlResponse.statusCode)))
            }
            
            // Now that all our prerequisites are fullfilled, we can take our data and try to translate it to our generic type of T.
            guard let data = data else { return }
            do {
                let users = try JSONDecoder().decode(T.self, from: data)
                completionOnMain(.success(users))
            } catch {
                debugPrint("Could not translate the data to the requested type. Reason: \(error.localizedDescription)")
                completionOnMain(.failure(error))
            }
                 
        }
        
        // Start the request
        urlSession.resume()
    }
    
    /*
        Request data from endpoint with Basic Auth
        - Parameters:
            - url: the URL
            - httpMethod: The HTTP Method to use, either get or post in this case
            - completion: The completion closure, returning a Result of either the generic type or an error
     */
    func requestWithBasicAuth<T: Decodable>(fromURL url: URL, withUserName: String, withPassword: String, httpMethod: HttpMethod = .get,
                                            completion: @escaping(Result<T, Error>) -> Void) {
        let completionOnMain: (Result<T, Error>) -> Void = { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
        
        let login = String(format: "%@:%@", withUserName, withPassword)
        let loginData = login.data(using: String.Encoding.utf8)!
        let base64Login = loginData.base64EncodedString()
        
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.method
        request.setValue("Basic \(base64Login)", forHTTPHeaderField: "Authorization")
        
        let urlSession = URLSession.shared.dataTask(with: request) { data, response, error in
            // First check if we got an error, if so we are not interested int he response or data.
            // Remember, and 404, 500, 501 http error code does not result in an error in URLSession,
            // it will only return an error here in case of e.g. Network timeout.
            if let error = error {
                completionOnMain(.failure(error))
                return
            }
            
            // Lets check the status code, we are only interested in results between 200 and 300 in statuscode. If the statuscode is anything
            // else we want to return the error with the statuscode that was returned. In this case, we do not care about the data.
            guard let urlResponse = response as? HTTPURLResponse else {
                return completionOnMain(.failure(ManagerErrors.invalidResponse))
            }
            
            if !(200..<300).contains(urlResponse.statusCode) {
                print(urlResponse.statusCode)
                print(urlResponse.description)
                return completionOnMain(.failure(ManagerErrors.invalidStatusCode(urlResponse.statusCode)))
            }
            
            // Now that all our prerequisites are fullfilled, we can take our data and try to translate it to our generic type of T.
            guard let data = data else { return }
            do {
                let users = try JSONDecoder().decode(T.self, from: data)
                completionOnMain(.success(users))
            } catch {
                debugPrint("Could not translate the data to the requested type. Reason: \(error.localizedDescription)")
                completionOnMain(.failure(error))
            }
                 
        }
        
        // Start the request
        urlSession.resume()
    }
    
}
