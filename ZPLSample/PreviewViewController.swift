//
//  PreviewViewController.swift
//  ZPLSample
//
//  Created by Joshua Ventocilla on 6/6/24.
//

import Foundation
import UIKit
import StarIO10

class PreviewViewController: UIViewController { 
    
    var imageData: Data?
    var printerID: String?
    var interface: InterfaceType?
    var starConnectionSettings: StarConnectionSettings?
    
    @IBOutlet weak var previewImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if imageData == nil {
            print("image is nil")
        } else {
            
            guard let label = imageData else {
                print("some guard")
                return
            }
            
            let imageUse = UIImage(data: label)
            
            previewImage.image = imageUse
        } // end 'if imageData == nil'
    } // end viewDidLoad()
    
    @IBAction func printButtonPressed(_ sender: Any) {
        
        guard let labelImage = imageData else {
            return
        }
        
        let label = UIImage(data: labelImage)
        
        if self.printerID == nil {
            self.alertDeviceNotConnected()
        } else {

            let starConnectionSettings = StarConnectionSettings(interfaceType: self.interface!, identifier: self.printerID!)
            let printer = StarPrinter(starConnectionSettings)
            
            DispatchQueue.main.async {
                self.showPrintingSpiner()
            }
        
            Task {
                do {
                    // Set the print data created in Step 1 to the command variable.
                    let command = StarXpandCommand.StarXpandCommandBuilder().addDocument(
                        StarXpandCommand.DocumentBuilder().addPrinter(
                            StarXpandCommand.PrinterBuilder()
                                .actionPrintImage(StarXpandCommand.Printer.ImageParameter(image: label!, width: 576))
                                .actionCut(.partial)
                        )
                    )
                    
                    /** UNCOMMENT FOR PAGE MODE PRINTING **/
//                    let command = StarXpandCommand.StarXpandCommandBuilder().addDocument(
//                        StarXpandCommand.DocumentBuilder().addPrinter(
//                            StarXpandCommand.PrinterBuilder()
//                                .addPageMode(
//                                    parameter: StarXpandCommand.Printer.PageModeAreaParameter(width: 72.0, height: 30.0),
//                                    builder: StarXpandCommand.PageModeBuilder()
//                                        .actionPrintRectangle(
//                                            StarXpandCommand.Printer.PageModeRectangleParameter(x: 5.0, y: 2.0, width: 62.0, height: 26.0)
//                                                .setRoundCorner(true)
//                                                .setCornerRadius(5.0)
//                                        )
//                                        .actionPrintImage(
//                                            StarXpandCommand.Printer.PageModeImageParameter(image: imageUse!, x: 0.0, y: 10.0, width: 500)
//                                        )
//                                        .styleVerticalPositionTo(5.0)
//                                        .styleHorizontalPositionTo(10.0)
//                                )
//                                .actionCut(.partial)
//                        )
//                    )
                    
                    try await printer.open()
                    
                    defer {
                        Task {
                            await printer.close()
                        }
                    }
                    
                    try await printer.print(command: command.getCommands())
                    
                    DispatchQueue.main.async {
                        self.removeSpinner()
                    }
                    
                } catch StarIO10Error.notFound(message: let message, errorCode: let errorCode) {
                    
                    // Printer not found.
                    // This may be due to the printer not being turned on or incorrect connection information.
                    DispatchQueue.main.async {
                        self.removeSpinner()
                    }
                    
                    self.alertNoDevicesFound()
                    
                } catch let error {
                    
                    DispatchQueue.main.async {
                        self.removeSpinner()
                    }
                    
                    var errorMessage = "Error: \(error)"
                    self.alertDeviceError(message: errorMessage)
                } // end let error
            } // end Task
        } // end else
    } // end printButtonPressed()
    
} // end PreviewViewController

